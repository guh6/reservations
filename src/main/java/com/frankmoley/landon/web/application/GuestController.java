package com.frankmoley.landon.web.application;

import com.frankmoley.landon.business.service.GuestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(value="/guests")
public class GuestController {
    @Autowired
    private GuestService guestService;

    @RequestMapping(method= RequestMethod.GET, value="/states")
    public String getGuestsForAllStates(Model model){
        Map<String, String> guests = this.guestService.getGuestInformation();
        model.addAttribute("guests", guests);
        return "guests";
    }
}
