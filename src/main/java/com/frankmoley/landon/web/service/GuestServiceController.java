package com.frankmoley.landon.web.service;

import com.frankmoley.landon.business.service.GuestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value="/api")
public class GuestServiceController {
    // It should get a list of Guest Names with States
    @Autowired
    private GuestService guestService;

    @RequestMapping(method = RequestMethod.GET, value="/allstates")
    public Map<String, String> getGuestsForAllStates(){
        return this.guestService.getGuestInformation();
    }
}
