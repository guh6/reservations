package com.frankmoley.landon.business.service;

import com.frankmoley.landon.data.entity.Guest;
import com.frankmoley.landon.data.repository.GuestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Service
public class GuestService {
    private GuestRepository guestRepository;

    @Autowired
    public GuestService(GuestRepository guestRepository){
        this.guestRepository = guestRepository;
    }

    public Map<String, String> getGuestInformation(){
        Map<String, String> result = new HashMap();
        Iterable<Guest> guests = this.guestRepository.findAll();
        guests.forEach(guest -> {
            result.put( guest.getFirstName() + " " + guest.getLastName(), guest.getState());
        });
        return result;
    }

}
