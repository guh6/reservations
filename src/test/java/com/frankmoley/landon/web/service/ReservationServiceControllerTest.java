package com.frankmoley.landon.web.service;

import com.frankmoley.landon.business.domain.RoomReservation;
import com.frankmoley.landon.business.service.ReservationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Created by guoyang.huang on 12/18/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(ReservationServiceController.class)
public class ReservationServiceControllerTest {
    @MockBean
    private ReservationService reservationService; // Mock this object that is used in the actual class. E.g., allow(ReservationService).to receive(:new, instance_double(ReservationService))

    @Autowired
    private MockMvc mockMvc; // Create this controller for the test, e.g., controller.request in Rails. POST(/)

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    @Test // it returns a list of rooms
    public void getReservationList() throws Exception {

        Date date = DATE_FORMAT.parse("2017-12-18");
        // Mock a reservation List
        List<RoomReservation> mockedReservations = new ArrayList<>();
        RoomReservation mockedRoomReservation = new RoomReservation();
        mockedRoomReservation.setDate(date);
        mockedRoomReservation.setRoomId(123);
        mockedRoomReservation.setGuestId(99);
        mockedReservations.add(mockedRoomReservation);
        // Stub - the return of the reservationService to the mock list
        // E.g., allow(reservationService).to receive(:getRoomReservation).with("2017-12-18).and_return(mockedReservation)
        given(reservationService.getRoomReservationsForDate("2017-12-18")).willReturn(mockedReservations);

        // Expectation -
        // E.g., get('/api/reservations/2017-12-18'); expect(response.code).to be(200); expect(response.body).to contain("Test, JUnit");
        this.mockMvc.perform(get("/api/reservations/2017-12-18"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"roomId\":123")))
                .andExpect(content().string(containsString("\"guestId\":99")));
    }

    @Test // it returns an empty list
    public void getEmptyReservationList() throws Exception {
        given(reservationService.getRoomReservationsForDate("2017-01-01")).willReturn(new ArrayList<>());
        // get('/api/reservations/2017-01-01'); expect(response.code).to be(200); expect(response.body).to eq("[]")
        this.mockMvc.perform(get("/api/reservations/2017-01-01"))
                .andExpect((status().isOk()))
                .andExpect(content().string("[]"));
    }

    @Test // it returns an empty list for a non-existent date
    public void getNonExistentReservationList() throws Exception {
        // get("/api/reservations/2017-01-11"); expect(response.code).to be(200); expect(response.body).to eq("[]")
        this.mockMvc.perform(get("/api/reservations/2017-01-11"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
//                .andDo(print());
    }

}
