package com.frankmoley.landon.business.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.swing.text.DateFormatter;

import static org.junit.Assert.assertEquals;

/**
 * Created by guoyang.huang on 12/18/2017.
 */
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
public class RoomReservationTest {
    @Test // it can set and get roomID
    public void gettersSettersRoomId(){
        /*
         * expect(rR.getRoomId()).to eq(10)
         */
        RoomReservation rR = new RoomReservation();
        rR.setRoomId(10);
        assertEquals(rR.getRoomId(), 10);
    }

    @Test // it can set and retrieve date correctly
    /*
     * expect(date).to eq("2017-01-01"); expect(date).to eql(date)
     */
    public void gettersSettersDate() throws ParseException
    {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = df.parse("2017-01-01");

        RoomReservation rR = new RoomReservation();
        rR.setDate(date);
        assertEquals(rR.getDate(),date);
        assertEquals(rR.getDate().toString(), "Sun Jan 01 00:00:00 EST 2017");
    }

}
