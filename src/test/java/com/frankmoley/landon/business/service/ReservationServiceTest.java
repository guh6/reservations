package com.frankmoley.landon.business.service;

import com.frankmoley.landon.business.domain.RoomReservation;
import com.frankmoley.landon.data.entity.Guest;
import com.frankmoley.landon.data.entity.Reservation;
import com.frankmoley.landon.data.entity.Room;
import com.frankmoley.landon.data.repository.GuestRepository;
import com.frankmoley.landon.data.repository.ReservationRepository;
import com.frankmoley.landon.data.repository.RoomRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.mockito.BDDMockito.given;
import org.hamcrest.collection.IsEmptyCollection;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;

import static org.hamcrest.number.OrderingComparison.greaterThanOrEqualTo;
import static org.hamcrest.number.OrderingComparison.lessThan;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by guoyang.huang on 12/19/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class ReservationServiceTest {

    @MockBean
    private RoomRepository roomRepository;
    @MockBean
    private GuestRepository guestRepository;
    @MockBean
    private ReservationRepository reservationRepository;


    @Test // it should return room reservations for date
    /*
     * roomRepository = instance_double("com.frankmoley.landon.business.service::RoomRepository")
     * guestRepository = instance_double("com.frankmoley.landon.business.service::GuestRepository")
     * reservationRepository = instance_double("com.frankmoley.landon.business.service::ReservationRepository")
     * rooms = [thing]
     * guests = [thing]
     * room_reservations = [thing]
     * allow(roomRepository).to receive(:findAll()).and_return(rooms)
     * allow(guestRepository).to receive(:findOne(123)).and_return(guest)
     * allow(reservationRepository).to receive(:findByDate).with(date).and_return(reservations)
     *
     * expect(rS.getReservationsForDate(date)).to eq(...)     *
     */
    public void getRoomReservationsForDate() throws ParseException{
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2017-01-01");
        // 1. Replace Iterable<Room> with a Collection object that allows for #forEach method
        Collection<Room> rooms = new ArrayList<>();
        Room r1 = new Room();
        r1.setId(5);
        r1.setName("a room");
        r1.setNumber("10A");
        rooms.add(r1);
        // 2. Create a reservation collection
        List<Reservation> reservations = new ArrayList<Reservation>();
        Reservation re1 = new Reservation();
        re1.setGuestId(123);
        re1.setDate(new java.sql.Date(date.getTime()));
        re1.setId(5);
        reservations.add(re1);
        // 3. Create a guest
        Guest g1 = new Guest();
        g1.setFirstName("Gu");
        g1.setLastName("H");
        g1.setId(123);

        given(roomRepository.findAll()).willReturn(rooms);
        given(guestRepository.findOne(123L)).willReturn(g1);
        given(reservationRepository.findByDate(new java.sql.Date(date.getTime()))).willReturn(reservations);
        ReservationService rS = new ReservationService(roomRepository, guestRepository, reservationRepository);

        List<RoomReservation> result = rS.getRoomReservationsForDate("2017-01-01");

        Assert.notEmpty(result);
        org.junit.Assert.assertEquals(result.get(0).getFirstName(), "Gu");

        //hamcrest assertions
        assertThat(result.size(), is(1));
    }


}

